FROM nodejs-base

RUN git clone https://gitlab.com/adriguez/sensor-module-dht11.git

WORKDIR /src/sensor-module-dht11/lib-sensor

RUN tar zxvf bcm2835-1.55.tar.gz
WORKDIR /src/sensor-module-dht11/lib-sensor/bcm2835-1.55
RUN ./configure
RUN make
RUN make install

WORKDIR /src/sensor-module-dht11/app

RUN npm install

CMD [ "npm", "start" ]