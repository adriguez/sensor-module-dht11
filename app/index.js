//var sensorLib = require("node-dht-sensor");
const readSensor = require('./lib/sensor.js');
const { parse } = require('url');
const NO_PROXY = process.env.NO_PROXY = 'localhost';
const HOST_REQUEST = 'http://localhost';
const logger = require('./lib/logger.js');

module.exports = async request => {
  
  const { query } = parse(request.url, true);

  logger('info', '***** INPUT RECIBIDO: ' + JSON.stringify(query));

  if(Object.keys(query).length > 0){
    if (!query.idSensor || !query.nameSensor || !query.gpio) {
      const error = new ReferenceError(
        `Faltan parametros por informar. Ejemplo: idSensor:1, nameSensor:Sensor Exterior Everis, gpio:4`
      );
      error.statusCode = 400;
      throw error;

    }else{
      return await readSensor(query);
    }

  }
};